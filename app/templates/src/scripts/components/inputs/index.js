import React from 'react'

class TextInput extends React.Component {
  _handleChange(model,e){
    this.props.onChange(e,{type:model, value:e.target.value})
  }
  render(){
    return (
      <input type={this.props.type} onChange={this._handleChange.bind(this,this.props.model)} defaultValue={this.props.defaultValue} />
    )
  }
}

export {TextInput}