import React from 'react'

const IconButton = ({icon, type,onClick, disabled}) => (
  <button onClick={onClick} className={`ui icon button ${type} ${disabled}`}>
    <i className={`${icon} icon`}></i>
  </button>
)

export {IconButton}