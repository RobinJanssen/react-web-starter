import React from 'react'

import { TextInput } from '../inputs'

const Form = ({children}) => (
  <div className="ui form">
    {children}
  </div>
)

const FormRow = ({children,className}) => (
  <div className={className}>
    {children}
  </div>
)

const FormField = (props)  => (
  <div className="field">
    <label htmlFor="">{props.label}</label>
    <TextInput {...props} />
  </div>
)

export {Form, FormField, FormRow}