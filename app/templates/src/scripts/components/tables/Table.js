import React from 'react'
import {Link} from 'react-router'

require('../../../styles/tables.scss')

const Table = ({headers, rows, linkProp}) => (
  <table className="ui striped table">
    <thead>
      <tr>
        {headers.map((header,idx)=>{
          return (
            <th key={idx}>
              {header}
            </th>
          )
        })}
      </tr>
    </thead>
    <tbody>
      {rows.map((row,idx)=>{
        return (
          <TableRow linkProp={linkProp} key={idx} rowItems={row} />
        )
      })}
    </tbody>
  </table>
)

const TableRow = ({rowItems}) => {
  if (rowItems)
    return (
      <tr>
        {
          Object.keys(rowItems)
            .filter(key=>key!=='_id') //Skip the _id as a column
            .map((key,idx) => (
              <td key={idx}>
                <Link to={`/movies/${rowItems._id}`}>{rowItems[key]}</Link>
              </td>
            ))
        }
      </tr>
    )
}

export {Table}